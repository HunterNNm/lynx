<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\StoreItemRequest;
use App\Http\Resources\ItemCollection;
use App\Item;
use App\Services\ItemService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\Response;

class ItemController extends Controller
{
    /**
     * @var ItemService
     */
    private $itemService;

    /**
     * ItemController constructor.
     * @param ItemService $itemService
     * @param Item $item
     */
    public function __construct(ItemService $itemService, Item $item)
    {
        $this->middleware('auth:api');
        $this->itemService = $itemService;
        $this->authorizeResource(Item::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return new ItemCollection($this->itemService->currentUserItemPaginate());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreItemRequest $request)
    {
        if ($this->itemService->create($request->validated())) {
            return response()->json([], Response::HTTP_CREATED);
        }

        return response()->json([], Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Item $item
     * @return \Illuminate\Http\Response
     */
    public function show(Item $item)
    {
        return new \App\Http\Resources\Item($item);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Item $item
     * @return \Illuminate\Http\Response
     */
    public function update(StoreItemRequest $request, Item $item)
    {
        if ($this->itemService->update($item, $request->validated())) {
            return response()->json([], Response::HTTP_OK);
        }
        return response()->json([], Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Item $item
     * @return \Illuminate\Http\Response
     */
    public function destroy(Item $item)
    {
        if ($this->itemService->destroy($item)) {
            return response()->json([], Response::HTTP_NO_CONTENT);
        }
        return response()->json([], Response::HTTP_INTERNAL_SERVER_ERROR);
    }
}
