<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphTo;

class Change extends Model
{
    public const OLD_VALUE_COLUMN = 'old_value';
    public const NEW_VALUE_COLUMN = 'new_value';

    protected $casts = [
        'old_value' => 'array',
        'new_value' => 'array',
    ];

    protected $fillable = [
        'user_id', 'old_value', 'new_value', 'ip'
    ];

    /**
     * @return MorphTo
     */
    public function changeable(): MorphTo
    {
        return $this->morphTo();
    }

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
