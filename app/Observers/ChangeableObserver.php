<?php

namespace App\Observers;

use App\Change;
use Illuminate\Database\Eloquent\Model;

class ChangeableObserver
{
    /**
     * Handle the change "updated" event.
     *
     * @param Model $model
     * @return void
     */
    public function updated(Model $model): void
    {
        $updatedChanges = $model->getChanges();
        $oldValues = $model->getOriginal();
        $writingChanges = [];
        foreach ($updatedChanges as $field => $value) {
            if ($this->isNeedToWrite($model, $field)) {
                $writingChanges[Change::OLD_VALUE_COLUMN][$field] = $oldValues[$field];
                $writingChanges[Change::NEW_VALUE_COLUMN][$field] = $updatedChanges[$field];
            }
        }
        if (\count($writingChanges) > 0) {
            $model->changes()->create(array_merge(['user_id' => auth()->id(), 'ip' => request()->ip()], $writingChanges));
        }
    }

    /**
     * Handle the change "deleted" event.
     *
     * @param Model $model
     * @return void
     */
    public function deleted(Model $model): void
    {
        $model->changes()->delete();
    }

    /**
     * @param Model $model
     * @param string|null $field
     * @return bool
     */
    private function isNeedToWrite(Model $model, string $field = null): bool
    {
        return \in_array($field, $model->getChangeableFields(), true) && !\in_array($field, $model->getHidden(), true);
    }
}
