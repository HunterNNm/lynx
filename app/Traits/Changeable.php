<?php

namespace App\Traits;

use App\Change;
use App\Observers\ChangeableObserver;
use Illuminate\Database\Eloquent\Relations\MorphMany;

trait Changeable
{
    /**
     * @return MorphMany
     */
    public function changes(): MorphMany
    {
        return $this->morphMany(Change::class, 'changeable');
    }

    public static function bootChangeable(): void
    {
        static::observe(new ChangeableObserver());
    }

    /**
     * @return array
     */
    public function getChangeableFields(): array
    {
        return $this->changeableFields ?? [];
    }
}