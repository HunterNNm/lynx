<?php

namespace App;

use App\Traits\Changeable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Item extends Model
{
    use Changeable;

    protected $changeableFields = ['name', 'key'];

    /**
     * @var array
     */
    protected $fillable = ['name', 'key'];

    protected $hidden = ['user_id'];

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
