<?php

namespace App\Services;

use App\Item;

class ItemService
{
    /**
     * @var Item
     */
    private $item;

    /**
     * ItemService constructor.
     * @param Item $item
     */
    public function __construct(Item $item)
    {
        $this->item = $item;
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function create(array $data)
    {
        return \Auth::user()->items()->create($data);
    }

    /**
     * @param Item $item
     * @param array $data
     * @return mixed
     */
    public function update(Item $item, array $data)
    {
        return $item->update($data);
    }

    /**
     * @param Item $item
     * @return mixed
     * @throws \Exception
     */
    public function destroy(Item $item)
    {
        return $item->delete();
    }

    /**
     * @param int $limit
     * @return mixed
     */
    public function currentUserItemPaginate(int $limit = 15)
    {
        return $this->item->where('user_id', \Auth::id())->paginate($limit);
    }
}