<?php

namespace App\Policies;

use App\User;
use App\Item;
use Illuminate\Auth\Access\HandlesAuthorization;

class ItemPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the item.
     *
     * @param  \App\User  $user
     * @param  \App\Item  $item
     * @return mixed
     */
    public function view(User $user, Item $item)
    {
        return $this->isUserItem($user, $item);
    }

    /**
     * Determine whether the user can create items.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->is_active;
    }

    /**
     * Determine whether the user can edit the item.
     *
     * @param  \App\User $user
     * @param  \App\Item $item
     * @return bool
     */
    public function edit(User $user, Item $item): bool
    {
        return $this->isUserItem($user, $item);
    }

    /**
     * Determine whether the user can update the item.
     *
     * @param  \App\User $user
     * @param  \App\Item $item
     * @return bool
     */
    public function update(User $user, Item $item): bool
    {
        return $this->isUserItem($user, $item);
    }

    /**
     * Determine whether the user can delete the item.
     *
     * @param  \App\User $user
     * @param  \App\Item $item
     * @return bool
     */
    public function delete(User $user, Item $item): bool
    {
        return $this->isUserItem($user, $item);
    }

    /**
     * @param User $user
     * @param Item $item
     * @return bool
     */
    private function isUserItem(User $user, Item $item): bool
    {
        return (int)$user->id === (int)$item->user_id;
    }
}
