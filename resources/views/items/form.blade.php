@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <form method="post"
                      action="{{ isset($item) ? route('items.update', $item->id) : route('items.store') }}">
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" id="name" aria-describedby="emailHelp"
                               placeholder="Enter name" name="name"
                               value="{{ isset($item) && null === old('name') ? $item->name : old('name') }}">
                        @if ($errors->has('name'))
                            <small class="form-text text-muted">{{ $errors->first('name') }}</small>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="key">Key</label>
                        <input type="text" class="form-control" id="key" placeholder="Enter key(max 25)" name="key"
                               value="{{ isset($item) && null === old('key') ? $item->key : old('key') }}">
                        @if ($errors->has('key'))
                            <small class="form-text text-muted">{{ $errors->first('key') }}</small>
                        @endif
                    </div>
                    <button type="submit" class="btn btn-primary">@if(isset($item)) Обновить @else Добавить @endif</button>
                    @csrf
                    @if(isset($item))
                        @method('PUT')
                    @endif
                </form>
            </div>
        </div>
    </div>
@endsection
