@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12 justify-content-end d-flex">
                <a class="btn btn-primary d-flex mb-1" href="{{ route('items.create') }}" role="button">Добавить
                    предмет</a>
            </div>
            <div class="col-md-12">
                @if(isset($items) && count($items) > 0)
                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">First</th>
                            <th scope="col">Last</th>
                            <th scope="col">Handle</th>
                            <th scope="col">Handle</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($items as $item)
                            <tr>
                                <th>{{ $item->id }}</th>
                                <td><a href="{{ route('items.show', $item->id) }}">{{ str_limit($item->name, 25) }}</a>
                                </td>
                                <td>{{ str_limit($item->key, 15) }}</td>
                                <td>{{ $item->created_at }}</td>
                                <td>{{ $item->updated_at }}</td>
                                <td><a href="{{ route('items.edit', $item->id) }}"
                                       class="btn btn-link">Редактировать</a></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @else
                    <div class="h4 text-center">Записи не найдены</div>
                @endif
            </div>
        </div>
    </div>
@endsection
