@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <p class="h3">Информация о предмете</p>
            </div>
            <div class="col-md-12">
                <div class="row">
                    <div class="col-2 text-right">
                        <p class="h5">Name:</p>
                    </div>
                    <div class="col-10">
                        <p>{{ $item->name }}</p>
                    </div>
                    <div class="col-2 text-right">
                        <p class="h5">Key:</p>
                    </div>
                    <div class="col-10">
                        <p>{{ $item->key }}</p>
                    </div>
                </div>
                @can('update', $item)
                    <div class="">
                        <form action="{{ route('items.destroy', $item->id) }}" method="post">
                            <a href="{{ route('items.edit', $item->id) }}" class="btn btn-warning">Редактировать</a>
                            <button type="submit" class="btn btn-danger">Удалить</button>
                            @method('delete')
                            @csrf
                        </form>
                    </div>
                @endcan
            </div>
        </div>
        @if(count($item->changes) > 0)
            <div class="row">
                <div class="col-md-12 text-center mt-2 mb-1">
                    <p class="h3">Изменения предмета</p>
                </div>
            </div>
            <div class="row">
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">Имя пользователя</th>
                        <th scope="col">Поле</th>
                        <th scope="col">Старое значение</th>
                        <th scope="col">Новое значение</th>
                        <th scope="col">IP</th>
                        <th scope="col">Дата</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($item->changes as $change)
                        @foreach($change->old_value as $field => $value)
                            <tr>
                                <td>{{ $change->user->name }}</td>
                                <td>{{ $field }}</td>
                                <td>{{ $change->old_value[$field] }}</td>
                                <td>{{ $change->new_value[$field] }}</td>
                                <td>{{ $change->ip }}</td>
                                <td>{{ $change->created_at }}</td>
                            </tr>
                        @endforeach
                    @endforeach
                    </tbody>
                </table>
            </div>
        @endif
    </div>
@endsection
