<?php

namespace Tests\Feature;

use App\Item;
use App\User;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ItemTest extends TestCase
{
    use RefreshDatabase;

    private const ITEM_TABLE_NAME = 'items';
    private const CHANGES_TABLE_NAME = 'changes';

    private const VALID_DATA = [
        'name' => 'Valid name',
        'key' => 'Valid key'
    ];

    private const INVALID_DATA = [
        'name' => 'Name',
        'key' => 'Key'
    ];

    private const NEW_DATA = [
        'name' => 'New name',
        'key' => 'New key'
    ];

    /**
     * @var User
     */
    private $user;

    public function setUp()
    {
        parent::setUp();

        $this->user = factory(User::class)->create();
    }

    public function testIndexNotAuthRedicrect()
    {
        $response = $this->get(route('home'));
        $response
            ->assertStatus(Response::HTTP_FOUND)
            ->assertRedirect(route('login'));
    }

    public function testIndexAuthResponseOk()
    {
        $response = $this->actingAs($this->user)->get(route('home'));
        $response->assertStatus(Response::HTTP_OK);
    }

    public function testCreateItemFromForm()
    {
        //success create
        $response = $this->actingAs($this->user)
            ->post(route('items.store'), self::VALID_DATA);

        $response
            ->assertStatus(Response::HTTP_FOUND)
            ->assertRedirect(route('items.index'));

        $this->assertDatabaseHas(self::ITEM_TABLE_NAME, self::VALID_DATA);

        //bad validation rule
        $response = $this->actingAs($this->user)
            ->withSession(['_previous.url' => route('items.create')])
            ->post(route('items.store'), self::INVALID_DATA);
        $response
            ->assertStatus(Response::HTTP_FOUND)
            ->assertRedirect(route('items.create'))
            ->assertSessionHasErrors(array_keys(self::INVALID_DATA));

        $this->assertDatabaseMissing(self::ITEM_TABLE_NAME, self::INVALID_DATA);
    }

    public function testUpdateItemFromFront()
    {
        $item = $this->user->items()->create(self::VALID_DATA);

        //bad data
        $this->assertDatabaseHas(self::ITEM_TABLE_NAME, self::VALID_DATA);
        $response = $this->actingAs($this->user)
            ->withSession(['_previous.url' => route('items.edit', $this->user->id)])
            ->put(route('items.update', $item->id), self::INVALID_DATA);
        $response
            ->assertStatus(Response::HTTP_FOUND)
            ->assertRedirect(route('items.edit', $item->id))
            ->assertSessionHasErrors(array_keys(self::INVALID_DATA));

        //good new data
        $response = $this->actingAs($this->user)
            ->put(route('items.update', $item->id), self::NEW_DATA);
        $response
            ->assertStatus(Response::HTTP_FOUND)
            ->assertRedirect(route('items.index'));
        $this->assertDatabaseHas(self::ITEM_TABLE_NAME, self::NEW_DATA);
    }

    public function testDeleteItemFromFront()
    {
        $item = $this->user->items()->create(self::VALID_DATA);
        $this->assertDatabaseHas(self::ITEM_TABLE_NAME, self::VALID_DATA);

        $badUser = factory(User::class)->create();
        $response = $this->actingAs($badUser)->delete(route('items.destroy', $item->id));
        $response->assertStatus(Response::HTTP_FORBIDDEN);

        $response = $this->actingAs($this->user)->delete(route('items.destroy', $item->id));
        $response
            ->assertStatus(Response::HTTP_FOUND)
            ->assertRedirect(route('items.index'));
        $this->assertDatabaseMissing(self::ITEM_TABLE_NAME, self::VALID_DATA);
    }

    public function testGetUserItemsFromApi()
    {
        $items = $this->user->items()->saveMany(factory(Item::class, 10)->make());

        $secondUser = factory(User::class)->create();
        $secondUserItems = $secondUser->items()->save(factory(Item::class)->make());

        $response = $this->json('GET', route('api.items.index'));
        $response->assertStatus(Response::HTTP_UNAUTHORIZED);

        $response = $this->json('GET', route('api.items.index'), ['api_token' => $this->user->api_token]);
        $response
            ->assertStatus(Response::HTTP_OK)
            ->assertJsonStructure([
                'data' => [
                    '*' => [
                        'id',
                        'name',
                        'key',
                        'created_at',
                        'updated_at',
                    ]
                ]
            ])
            ->assertJsonCount(10, 'data')
            ->assertJsonFragment([
                'name' => $items->first()->name,
                'key' => $items->first()->key
            ])
            ->assertJsonMissing([
                'name' => $secondUserItems->name,
                'key' => $secondUserItems->key
            ]);
    }

    public function testGetUserItemFromApi()
    {
        $item = $this->user->items()->save(factory(Item::class)->make());

        $secondUser = factory(User::class)->create();
        $secondUserItems = $secondUser->items()->save(factory(Item::class)->make());

        $response = $this->json('GET', route('api.items.show', $item->id), ['api_token' => $this->user->api_token]);
        $response
            ->assertStatus(Response::HTTP_OK)
            ->assertJsonFragment([
                'name' => $item->name,
                'key' => $item->key
            ]);

        $response = $this->json('GET', route('api.items.show', $secondUserItems->id),
            ['api_token' => $this->user->api_token]);
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    public function testCreateItemsApi()
    {
        $response = $this->json('POST', route('api.items.store'), array_merge(self::VALID_DATA));
        $response->assertStatus(Response::HTTP_UNAUTHORIZED);

        $response = $this->json('POST', route('api.items.store'),
            array_merge(self::VALID_DATA, ['api_token' => $this->user->api_token]));
        $response->assertStatus(Response::HTTP_CREATED);
        $this->assertDatabaseHas(self::ITEM_TABLE_NAME, self::VALID_DATA);

        $response = $response = $this->json('POST', route('api.items.store'),
            array_merge(self::INVALID_DATA, ['api_token' => $this->user->api_token]));
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertDatabaseMissing(self::ITEM_TABLE_NAME, self::INVALID_DATA);
    }

    public function testUpdateUserItemsApi()
    {
        $item = $this->user->items()->save(factory(Item::class)->make());

        $response = $this->json('PUT', route('api.items.update', $item->id),
            array_merge(self::NEW_DATA, ['api_token' => $this->user->api_token]));
        $response->assertStatus(Response::HTTP_OK);
        $this->assertDatabaseHas(self::ITEM_TABLE_NAME, self::NEW_DATA);
        $this->assertDatabaseHas(self::CHANGES_TABLE_NAME, [
            'user_id' => $this->user->id,
            'changeable_id' => $item->id,
            'old_value' => json_encode(['name' => $item->name, 'key' => $item->key]),
            'new_value' => json_encode(['name' => self::NEW_DATA['name'], 'key' => self::NEW_DATA['key']]),
        ]);

        $secondUser = factory(User::class)->create();
        $secondUserItems = $secondUser->items()->save(factory(Item::class)->make());

        $response = $this->json('PUT', route('api.items.update', $secondUserItems->id),
            array_merge(self::VALID_DATA, ['api_token' => $this->user->api_token]));
        $response->assertStatus(Response::HTTP_FORBIDDEN);
        $this->assertDatabaseMissing(self::ITEM_TABLE_NAME, self::VALID_DATA);
    }

    public function testDeleteUserItemsApi()
    {
        $item = $this->user->items()->save(factory(Item::class)->make());

        $this->json('PUT', route('api.items.update', $item->id),
            array_merge(self::NEW_DATA, ['api_token' => $this->user->api_token]));
        $this->assertDatabaseHas(self::CHANGES_TABLE_NAME, [
            'user_id' => $this->user->id,
            'changeable_id' => $item->id,
        ]);

        $response = $this->json('DELETE', route('api.items.destroy', $item->id),
            ['api_token' => $this->user->api_token]);
        $response->assertStatus(Response::HTTP_NO_CONTENT);
        $this->assertDatabaseMissing(self::ITEM_TABLE_NAME, [
            'name' => $item->name,
            'key' => $item->key
        ]);

        $this->assertDatabaseMissing(self::CHANGES_TABLE_NAME, [
            'user_id' => $this->user->id,
            'changeable_id' => $item->id,
        ]);

        $secondUser = factory(User::class)->create();
        $secondUserItems = $secondUser->items()->save(factory(Item::class)->make());

        $response = $this->json('DELETE', route('api.items.update', $secondUserItems->id),
            ['api_token' => $this->user->api_token]);
        $response->assertStatus(Response::HTTP_FORBIDDEN);
        $this->assertDatabaseHas(self::ITEM_TABLE_NAME, [
            'name' => $secondUserItems->name,
            'key' => $secondUserItems->key,
        ]);
    }
}
